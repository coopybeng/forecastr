library(forecastR)
data(mvts1, mvts1m, mvts20, mvts20m,
     GenericCalendarVariables.W,calendar.445,calendar.weekly)

roll.opts = DefaultRollOpts() 
roll.opts = within(roll.opts, {
    TRACE = 0.3
    best.fit = FALSE
    outfile = ""
    cores = 16
    cluster.type = "FORK"
})

model.opts = DefaultModelOpts()
model.opts = within(model.opts, {
    reoptimise = TRUE
    reestimate = FALSE 
    optim.method = "CSS"
})

forecast.methods = c("REG-STEP","SNAIVE","NAIVE", "ETS", "ES","REG-ARIMA", "ARIMA")
#forecast.methods = "ES"
#forecast.methods = "REG-ARIMA"

#x=ForecastMain(mvts20,forecast.methods,model.opts,roll.opts)
probs = c("00-01-49000-00639", "00-01-28200-00869", "00-01-28200-10012", 
          "00-01-28200-10690", "00-01-37000-92291", "00-01-51000-01261")
test.data = mvts20[fc.item %in% probs]

foreach(method = forecast.methods) %do%
{
    print(method)    
    x = system.time({fc.w.20 = ForecastMain(mvts20, 
                                forecast.methods = method, 
                                model.opts = model.opts, roll.opts = roll.opts)
    })
    print(x)
    
}

fc.w.20.reest = copy(fc.w.20)
err.reopt = SummariseErrors(CalculateErrors(fc.w.20.reopt))[,type := "REOPT"]
err.reest = SummariseErrors(CalculateErrors(fc.w.20.reest))[,type := "REEST"]

err.all = rbindlist(list(err.reest,err.reopt))
err.melt = melt(err.all,id.vars=c("type", "fc.item"), measure.vars="smape")
dcast(err.melt, method+variable~type, mean)

Rprof(tmp <- tempfile())
#roll.opts = DefaultRollOpts()
#model.opts = DefaultModelOpts()
method = "ARIMA"
#method = "REG-STEP"
x = Roll(mvts20, forecast.methods = method, model.opts=model.opts, roll.opts=roll.opts)
Rprof()
summaryRprof(tmp)

y = ts(rnorm(312,10,2),freq=52,start = c(2001,1), end = c(2006, 52))
o1 = round(0.66 * length(y))
index(y)
fit = HoltWinters(y[1:o1])
lapply(o = o1:length(y))


for (method in forecast.methods) {
    system.time({x = Roll(mvts1, forecast.methods = method, model.opts=model.opts, roll.opts=roll.opts)})    
}

system.time(ets(gas))


x
