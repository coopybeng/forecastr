PlotForecast = function(act, fc, t, h, include.errors) {
    
}

PlotForecastsMVTS = function(mvts, fc, h.show = 1) {
    
    act = mvts[period < min(fc[h == h.show]$t), list(t = period, variable = "act", value = units.adj)]
    
    plot.data = fc[h == h.show, list(t,fc,act)]
    
    plot.data.melt = melt(data = plot.data, value.name = "value", variable.name = "series", id.vars = "t")
    plot.data = rbindlist(l=list(plot.data.melt, act))
    
    setkeyv(plot.data, "t")
    calendar = GetCalendarDates("W",start.period = min(plot.data$t), end.period = max(plot.data$t))
    
    y.max = max(plot.data$value)
    
    # get the calendar dates for this time series
    plot.data = plot.data[calendar]
    ggplot(plot.data, aes( y = value, x = Date, colour=series)) + geom_line() +geom_point() +
        scale_x_date(breaks = date_breaks("years"), minor_breaks=date_breaks("4 weeks") , labels = date_format("%b-%Y")) + 
        ylim(c(0,1.05*y.max)) + theme_bw()
}

GetCalendarDates = function(periodicity, start.period, end.period, output.as="data.table") {
    calendar = if (periodicity=="W") {
        calendar.weekly[period %in% start.period:end.period, list(period, Date=period.date)]
    } else if (periodicity == "445") {
        calendar.445[period %in% start.period:end.period, list(period, Date=period.date)]
    } 
    setkeyv(calendar,"period")
    return(calendar)
}