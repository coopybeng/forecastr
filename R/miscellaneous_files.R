#dat = ReadForecastFile_all()

ReadForecastFile_all = function(pth = "~/data/iri/output/forecasts/original/", 
                                pattern = "FORECAST*", 
                                category=NULL, 
                                level=1) {
    library(stringr)
    if (!is.null(category)) pattern = paste0(pattern, category, "*")
    files = dir(pth, pattern = pattern)[-(1:8)]#[c(1,20,15,46)]
    fc.all = rbindlist (lapply(files, FUN = function(fil) { ReadForecastFile(pth, fil) }))
    
    fc.all[, periodicity:="W"]
    fc.all[periodicity.gen==12, periodicity:="M"]
    
    names.to.keep = c( "periodicity", "method", "fc.item",
                       "o", "h", "t", "fc", "act",
                       "fc.naive", "fc.snaive", 
                       "mae.naive", "mae.snaive")
    
    fc.all = fc.all[, .SD, .SDcols=names.to.keep]
    #saveRDS(fc.in.2,"~/data/iri/output/forecasts/level3.mw.ets_reg.rds")
    
    #fc.all[, level:=factor(level,labels = c("ITEM","CHAIN","STORE"))]
    #fc.all[, periodicity:=factor(periodicity, labels = c("MONTHLY","WEEKLY"))]
    setkeyv(fc.all, "fc.item")
    
    saveRDS(fc.all,"~/data/iri/output/forecasts/level3.mw.ets_reg.rds")
    return(fc.all)
}

ReadForecastFile = function(pth, fil) {
    
    # we need to retrieve the data
    print(fil)
    dat = readRDS(paste(pth, fil, sep = "/"))
    
    #dat[, item_count:=1]
    dat[, level:=str_count(fc.item, "/") + 1]
    dat = dat[level==1]
    setcolorder(dat, c("level", names(dat)[1:(length(names(dat)) - 1)]))
    
    # need to get the values out of the file name to retrieve category, method, periodicity, horizon
    file.name.split = strsplit(gsub(".rds", "", fil), split = "_")
    
    dat.other = data.table (method          = file.name.split[[1]][2],
                            category        = file.name.split[[1]][6],
                            periodicity.gen = file.name.split[[1]][3],
                            periodicity.eval = file.name.split[[1]][3],
                            cycle           = file.name.split[[1]][4],
                            horizon         = file.name.split[[1]][5])
    data.table(dat.other, dat)
}

ReadForecasts = function(method, freq.ts, freq.cycle, h.max, category)
{
    pth.forecasts = "~/data/output/forecasts/"
    
    #if (method=="REG-STEP") fil = gsub("FORECAST_", replacement = "02_FORECAST_", x = fil)
    print(fil)
    ####### check file exists!!!!!!!
    if (file.exists(fil)) {
        fc = readRDS(fil)
        fc[, id:=1:.N]
        fc[,horizon.month:=GetHorizonMonth(freq.ts,h), by = id]
        return(fc)
    } else { 
        return (NULL)
    }
}
